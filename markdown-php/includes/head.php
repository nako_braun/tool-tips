<!DOCTYPE html>
<?php include('./vendor/PHP-Markdown-Lib-1-9-0/Michelf/MarkdownExtra.inc.php') ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    include('vendor/erusev/parsedown/Parsedown.php');
    $Parsedown = new Parsedown();
    ?>
    <main>

