<?php 
include 'includes/head.php';
?>  

<!-- Afficher un fichier markdown -->
<section>
    <?php
    $doc = file_get_contents('exemple.md');
    $exemple = $Parsedown->text($doc);
    echo '<article>' . $exemple . '</article>';
    ?>
</section>

<!-- Afficher plusieurs markdown depuis un dossier précis -->
<sections class="articles">
    <?php
    $directory = glob('posts/*.md');     
    rsort($directory);

    foreach($directory as $dir){
        $file = file_get_contents($dir);
        $split = explode("/",$dir);
        $titlePost = explode(".md",$split[1]);
        $post = $Parsedown->text($file);
        echo '<article class="post"><h1>' . $titlePost[0] . '</h1><div class="article">' . $post . '</div></article>';
    }
    ?>
</section>
    
<?php include 'includes/footer.php'; ?>