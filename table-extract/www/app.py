import files2ods 
import glob 
import os
import pathlib
from pyexcel_ods3 import save_data, get_data
import collections
import json
import yaml
from jinja2 import Environment, FileSystemLoader
import markdown

#---- GENERAL SITE STATIC ----
templatesDir = "templates/"
defaultTemplateFile = "main.html"
siteDir = "site"
port = 8888

#---- YAML SESSIONS INFOS ---- 
db_sessions = []
with open('data/sessions.yml', 'r') as file:
    sessions_yaml = yaml.safe_load_all(file)
    for sessions in sessions_yaml:
        if 'text' in sessions.keys():
            sessions['text'] = markdown.markdown(sessions['text'])
        db_sessions.append(sessions)
print(db_sessions)

#---- YAML BIOS INFOS ---- 
# db_bios = []
# with open('data/bios.yml', 'r') as file:
#     bios_yaml = yaml.safe_load_all(file)
#     for bios in bios_yaml:
#         if 'text' in bios.keys():
#             bios['text'] = markdown.markdown(bios['text'])
#         db_bios.append(bios)
# print(db_bios)

#---- FILES TABLE ---- 
# on récupère le contenu des tableurs
data_files = get_data("data/files.ods")
# je déclare ma nouvelle base de donnée qui sera rangée
db_items = []

# pour chaque page du tableur
for page in data_files:
    # print('-->', page)
    # ws['nameWs'] = page
    # print(ws)

    # je déclare un dictionnaire qui va comprendre mon nouveau tableau par page
    table = {}
    table[page] = []
    # je recupere les catégories et tout le contenu par ligne
    categories = data_files[page][0]
    rows = data_files[page]
    # on supprime la première ligne du tableur
    rows.pop(0)
    
    # je déclare un dicitonnaire qui va comprendre les élèments dans chacune de mes colonnes
    # pour chaque ligne
    for row in rows:
        column = {}

        # print(row)
        # je récupère la position de chaque élement de ma ligne
        for i, ro in enumerate(row):
            
            # je vérifie s'il correspond à la position d'une catégorie et l'ajoute dans mon nouveau dicitonnaire de colonne
            column[categories[i]] = ro
            # print(column) 
            
            # j'ajoute mon dictionnaire de colonnes rangées dans le dicitonnaire de mon nouveau tableau pour chacune de mes pages
        table[page].append(column)

    # print('----', table)
    for key, value in table.items():
        print('--------')
    # je sauve le tout dans ma nouvelle base de donnée
    db_items.append(table)

# print(db)
# for d in db:
#     # print(d.keys())
#     print(d.get('Burren'))   # print(row)

#---- GENERATION DU SITE STATIC ----
def templateToSite():
        file_loader = FileSystemLoader("templates")
        env = Environment(loader=file_loader)
        template = env.get_template("main.html")

        output = template.render(
            db_items=items,
            db_sessions=sessions,
            db_bios=bios
        )
        with open(siteDir + "/index.html", "w") as fh:
            fh.write(output)

templateToSite()