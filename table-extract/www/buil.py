import files2ods 
import glob 
import os
import pathlib
import json

from pyexcel_ods3 import save_data, get_data
import collections

_WS_FILES = pathlib.Path("content") 

def getODS(file):
    data = get_data(file)
    r = json.dumps(data)
    print(r[0])


def generateODS(): 
    data = collections.OrderedDict()
    for folder in glob.glob('content/*'):
        if os.path.isdir(folder):
            folder_name = folder.split('/')[-1]
            _files_list = list()
            for file in list(pathlib.Path(folder).rglob('*')):
                l = list()
                file_name = str(file).split('/')[-1]
                l.append(file_name)
                l.append(str(file))
                _files_list.append(l)
                data.update({folder_name: _files_list})

    save_data('data.ods', data)

if __name__ == '__main__':
    # generateODS()
    getODS('sources/template.ods')

